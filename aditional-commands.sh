#!/bin/bash

#Steps base on: 
#https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/#step-4-install-and-configure-gitlab-runner-on-the-ec2-instance

# Step #4: When you are connected successfully, run the following commands
mkdir -p /opt/gitlab-runner/{metadata,builds,cache}
curl -s "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt install -y gitlab-runner

#Step #5: Run this command with the GitLab URL and registration token you noted in step 1.
gitlab-runner register --url https://gitlab.com/ --registration-token ${REGISTRATION_TOKEN} --name ${RUNNER_NAME} --run-untagged --executor custom -n

#Step 5.5 Replace default file lines
sed -i 's/  \[runners.custom_build_dir\]//gi' /etc/gitlab-runner/config.toml
sed -i 's/  \[runners.cache\]//gi' /etc/gitlab-runner/config.toml
sed -i 's/    \[runners.cache.s3\]//gi' /etc/gitlab-runner/config.toml
sed -i 's/    \[runners.cache.gcs\]//gi' /etc/gitlab-runner/config.toml
sed -i 's/    \[runners.cache.azure\]//gi' /etc/gitlab-runner/config.toml
sed -i 's/  \[runners.custom\]//gi' /etc/gitlab-runner/config.toml
sed -i 's/    run_exec = ""//gi' /etc/gitlab-runner/config.toml

#Step #6: Run nano vim /etc/gitlab-runner/config.toml and add the following content:
cat << EOF >> /etc/gitlab-runner/config.toml
  builds_dir = "/opt/gitlab-runner/builds"
  cache_dir = "/opt/gitlab-runner/cache"
  [runners.custom]
    volumes = ["/cache", "/path/to-ca-cert-dir/ca.crt:/etc/gitlab-runner/certs/ca.crt:ro"]
    config_exec = "/opt/gitlab-runner/fargate"  
    config_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "config"]
    prepare_exec = "/opt/gitlab-runner/fargate"
    prepare_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "prepare"]
    run_exec = "/opt/gitlab-runner/fargate"
    run_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "run"]
    cleanup_exec = "/opt/gitlab-runner/fargate"
    cleanup_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "cleanup"]
EOF

#Step 8: Run vim /etc/gitlab-runner/fargate.toml and add the following content:
cat << EOF >> /etc/gitlab-runner/fargate.toml
LogLevel = "info"
LogFormat = "text"

[Fargate]
  Cluster = "${CLUSTER_NAME}"
  Region = "${REGION}"
  Subnet = "${SUBNET}"
  SecurityGroup = "${SECURITY_GROUP}"
  TaskDefinition = "${TASK}:${REVISION}"
  EnablePublicIP = true

[TaskMetadata]
  Directory = "/opt/gitlab-runner/metadata"

[SSH]
  Username = "root"
  Port = 22
EOF

#Step 9: Install the Fargate driver:
curl -Lo /opt/gitlab-runner/fargate "https://gitlab-runner-custom-fargate-downloads.s3.amazonaws.com/latest/fargate-linux-amd64"
chmod +x /opt/gitlab-runner/fargate