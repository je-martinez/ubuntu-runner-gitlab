#Download base image ubuntu 20.04
FROM ubuntu:20.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository
RUN apt update

# Install Depedendencies
RUN apt-get install -y nano && apt-get install -y curl

#Expose Running Port
EXPOSE 80

# -------------------------------------------------------------------------------------
# Execute a startup script.
# https://success.docker.com/article/use-a-script-to-initialize-stateful-container-data
# for reference.
# -------------------------------------------------------------------------------------
COPY aditional-commands.sh /usr/local/bin/aditional-commands.sh

RUN chmod +x /usr/local/bin/aditional-commands.sh

ENTRYPOINT ["/usr/local/bin/aditional-commands.sh"]
# RUN /usr/local/bin/aditional-commands.sh